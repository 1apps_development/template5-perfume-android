package com.workdo.perfume.ui.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.*
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.perfume.R
import com.workdo.perfume.adapter.*
import com.workdo.perfume.adapter.variant.VariantAdapter
import com.workdo.perfume.api.ApiClient
import com.workdo.perfume.databinding.ActProductDetailsBinding
import com.workdo.perfume.databinding.DlgConfirmBinding
import com.workdo.perfume.databinding.DlgFeedbackBinding
import com.workdo.perfume.model.*
import com.workdo.perfume.model.ExtensionFunctions.show
import com.workdo.perfume.remote.NetworkResponse
import com.workdo.perfume.ui.authentication.ActWelCome
import com.workdo.perfume.utils.Constants
import com.workdo.perfume.utils.ExtensionFunctions.hide
import com.workdo.perfume.utils.ExtensionFunctions.invisible
import com.workdo.perfume.utils.PaginationScrollListener
import com.workdo.perfume.utils.SharePreference
import com.workdo.perfume.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActProductDetails : AppCompatActivity() {
    private lateinit var _binding: ActProductDetailsBinding
    private lateinit var adapter: ViewPagerAdapter
    private lateinit var variantAdapter: VariantAdapter
    var rattingValue = "0"
    var product_id = ""
    var variant_id = ""
    var variantName = ""
    var finalPrice = ""
    var disCountPrice = ""
    var originalPrice = ""
    var productInfo = ProductInfo()
    var productStock = ""

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    var recentdataitem = RecentProductDataItem()
    private var recentProductdataList = ArrayList<RecentProductDataItem>()
    private lateinit var recentProductadapter: RecentProductAdapter
    private var manager: GridLayoutManager? = null
    private var isWishList = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActProductDetailsBinding.inflate(layoutInflater)

        setContentView(_binding.root)
        init()

    }


    private fun init() {

        manager = GridLayoutManager(this@ActProductDetails, 1, GridLayoutManager.HORIZONTAL, false)
        RecentProductpagination()

        if (Utils.isLogin(this@ActProductDetails)) {
            _binding.btnFeedback.show()

        } else {
            _binding.btnFeedback.hide()
        }

        initClickListeners()
        callProductDetailApi()

        if (SharePreference.getStringPref(this@ActProductDetails, SharePreference.cartCount).isNullOrEmpty()) {
            SharePreference.setStringPref(this@ActProductDetails, SharePreference.cartCount, "0")
        }


        _binding.btnFeedback.setOnClickListener {
            openFeedbackDialog()
        }
        _binding.btnMore.setOnClickListener {
            val contactUs = SharePreference.getStringPref(this@ActProductDetails, SharePreference.returnPolicy).toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun RecentProductpagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callRecentProductApi()
            }

        }
        _binding.rvRecentProducts.addOnScrollListener(paginationListener)
    }

    override fun onResume() {
        super.onResume()
        isLastPage = false
        isLoading = false
        currentPage = 1
        recentProductdataList.clear()
        Recentproductadapter(recentProductdataList)
        callRecentProductApi()

        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.cartCount)
                .toString()
    }

    private fun Recentproductadapter(recentProductdataList: ArrayList<RecentProductDataItem>) {
        _binding.rvRecentProducts.layoutManager = manager
        recentProductadapter =
            RecentProductAdapter(this@ActProductDetails,
                recentProductdataList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActProductDetails,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (recentProductdataList[i].inWhishlist == false) {
                            callWishlist(
                                "add", recentProductdataList[i].id, "resentproductList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                recentProductdataList[i].id,
                                "resentproductList",
                                i
                            )
                        }
                    } else {
                        startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                    }
                } else if (s == Constants.CartClick) {
                    val data = recentProductdataList[i]
                    if (!Utils.isLogin(this@ActProductDetails)) {
                        // guestUserAddToCart(data, recentProductdataList[i].id)
                        guestUserAddToCart(data,
                            recentProductdataList[i].id,
                            recentProductdataList[i].defaultVariantId.toString(),
                            true)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(this@ActProductDetails,
                                SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = recentProductdataList[i].id.toString()
                        addtocart["variant_id"] =
                            recentProductdataList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            recentProductdataList[i].id,
                            recentProductdataList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActProductDetails, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, recentProductdataList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvRecentProducts.adapter = recentProductadapter
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActProductDetails)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.userId).toString()
        //wishlist["product_id"] = id.toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    val i = Intent("refresh_data")
                    i.putExtra("filter", intent.getStringExtra("filter"))
                    setResult(RESULT_OK, i)
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "resentproductList" -> {
                                        recentProductdataList[position].inWhishlist = true
                                        recentProductadapter.notifyItemChanged(position)
                                    }
                                    "productlist" -> {
                                        isWishList = true
                                        _binding.ivLike.background = ResourcesCompat.getDrawable(
                                            resources,
                                            R.drawable.ic_wishlist,
                                            null)

                                    }

                                }
                            } else {
                                when (itemType) {
                                    "resentproductList" -> {
                                        recentProductdataList[position].inWhishlist = false
                                        recentProductadapter.notifyItemChanged(position)
                                    }
                                    "productlist" -> {
                                        isWishList = false
                                        _binding.ivLike.background = ResourcesCompat.getDrawable(
                                            resources,
                                            R.drawable.ic_diswishlist,
                                            null)

                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                            this@ActProductDetails.finish()
                            this@ActProductDetails.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun callRecentProductApi() {
        Utils.showLoadingProgress(this@ActProductDetails)
        val recentproduct = HashMap<String, String>()
        recentproduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(this@ActProductDetails, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetails)
                        .setRecentproductGuest(currentPage.toString(), recentproduct)
                } else {
                    ApiClient.getClient(this@ActProductDetails)
                        .setRecentproduct(currentPage.toString(), recentproduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val recentproduct = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {

                                _binding.rvRecentProducts.show()

                                this@ActProductDetails.currentPage =
                                    recentproduct?.currentPage!!.toInt()
                                this@ActProductDetails.total_pages =
                                    recentproduct.lastPage!!.toInt()
                                recentproduct.data?.let {
                                    recentProductdataList.addAll(it)
                                }

                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvRecentProducts.hide()
                            }
                            recentProductadapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                recentproduct?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                recentproduct?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(this@ActProductDetails)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    private fun openFeedbackDialog() {
        var dialog: Dialog? = null
        try {
            dialog?.dismiss()
            dialog = Dialog(this@ActProductDetails, R.style.AppCompatAlertDialogStyleBig)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val mInflater = DlgFeedbackBinding.inflate(layoutInflater)
            mInflater.apply {


                btnRateNow.setOnClickListener {
                    when {
                        ivRatting.rating.toInt() == 0 -> {
                            Utils.errorAlert(this@ActProductDetails, resources.getString(R.string.validation_alert))
                        }
                        edNote.text?.isEmpty() == true -> {
                            Utils.errorAlert(this@ActProductDetails, resources.getString(R.string.validation_alert))
                        }
                        edTitle.text?.isEmpty() == true -> {
                            Utils.errorAlert(this@ActProductDetails, resources.getString(R.string.validation_alert))
                        }
                        else -> {
                            val ratting = HashMap<String, String>()
                            ratting["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
                            ratting["user_id"] = SharePreference.getStringPref(this@ActProductDetails, SharePreference.userId).toString()
                            ratting["rating_no"] = ivRatting.rating.toInt().toString()
                            ratting["title"] = edTitle.text.toString()
                            ratting["description"] = edNote.text.toString()
                            ratting["theme_id"] = resources.getString(R.string.theme_id)
                            callProductRatting(ratting)
                            dialog.dismiss()
                        }
                    }
                }
                btnCancel.setOnClickListener {
                    dialog.dismiss()
                }

            }
            dialog.setContentView(mInflater.root)
            dialog.show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun callProductRatting(ratting: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .productRating(ratting)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            Utils.successAlert(this@ActProductDetails,
                                rattingResponse?.message.toString())
                        }
                        0 -> {
                            Utils.errorAlert(this@ActProductDetails,
                                rattingResponse?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))

                            this@ActProductDetails.finish()
                            this@ActProductDetails.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun setupDetailData(data: ProductDetailData) {
        _binding.tvProductName.text = data.productInfo?.name
        _binding.tvProductDesc.text = data.productInfo?.description.toString()
        _binding.tvProductPrice.text = Utils.getPrice(data.productInfo?.finalPrice.toString())
        _binding.tvDiscountPrice.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.currency)
                .toString().plus(Utils.getPrice(data.productInfo?.originalPrice.toString()))
        _binding.tvCurrency.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.currency)
                .toString()

        /*  val variantList = ArrayList<VariantItem>()
          data.variant?.let { variantList.addAll(it) }

          variantList.let { setupVariantAdapter(it) }
          Log.e("gson", Gson().toJson(variantList))
          variantList.let { callFirstPositionVariantCheck(it) }*/

        isWishList = data.productInfo?.inWhishlist ?: false
        if (data.productInfo?.inWhishlist == true) {
            _binding.ivLike.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_wishlist, null)
        } else {
            _binding.ivLike.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_diswishlist, null)
        }

        val imageList = ArrayList<ProductImageItem>()
        data.productImage?.let { imageList.addAll(it) }
        if (imageList.size == 0) {
            _binding.viewPager.hide()
            _binding.vieww.show()
        } else {
            _binding.viewPager.show()
            _binding.vieww.hide()
            adapterSetup(imageList)
            setCurrentIndicator(0)
        }
        _binding.ivratting.rating = data.productInfo?.averageRating?.toFloat() ?: 0.0f
        if (data.productReview?.size == 0) {
            _binding.rvReview.hide()
            _binding.tvTestimonials.hide()
            _binding.viewww.hide()
        } else {
            _binding.rvReview.show()
            _binding.tvTestimonials.show()
            _binding.viewww.show()
            val snapHelper: SnapHelper = PagerSnapHelper()
            _binding.rvReview.layoutManager =
                GridLayoutManager(this@ActProductDetails, 1, GridLayoutManager.HORIZONTAL, false)
            val adapter =
                data.productReview?.let {
                    RattingAdapter(this@ActProductDetails, it) { id: String, name: String ->
                    }
                }
            _binding.rvReview.adapter = adapter
            snapHelper.attachToRecyclerView(_binding.rvReview);
        }

        val otherDescriptionArray = data.productInfo?.otherDescriptionArray
        otherDescriptionArray?.removeAll { it.description?.isEmpty() == true }
        if (data.productInfo?.otherDescriptionArray?.size == 0) {
            _binding.rvDescription.hide()
        } else {
            _binding.rvDescription.show()

            _binding.rvDescription.apply {
                layoutManager = LinearLayoutManager(this@ActProductDetails)
                adapter = otherDescriptionArray?.let { DescriptionAdapter(it) }
            }
        }
    }


    private fun initClickListeners() {

        _binding.ivBack.setOnClickListener {
            finish()
        }
        _binding.clcart.setOnClickListener {
            startActivity(Intent(this@ActProductDetails, ActShoppingCart::class.java))
        }

        if (Utils.isLogin(this)) {
            _binding.ivLike.show()
        } else {
            _binding.ivLike.hide()
        }


        _binding.ivLike.setOnClickListener {
            val i = Intent("refresh_data")
            i.putExtra("filter", intent.getStringExtra("filter"))
            LocalBroadcastManager.getInstance(this).sendBroadcast(i)
            if (isWishList) {
                callWishlist("remove", product_id.toInt(), "productlist", 0)
            } else {
                callWishlist("add", product_id.toInt(), "productlist", 0)

            }
        }



        _binding.btnAddtoCard.setOnClickListener {
            Log.e("Stock", productStock)

            if (productStock == "0") {
                if (Utils.isLogin(this@ActProductDetails)) {
                    val notifyUser = HashMap<String, String>()
                    notifyUser["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        )
                            .toString()
                    notifyUser["product_id"] = product_id.toString()
                    notifyUser["theme_id"] = getString(R.string.theme_id)
                    notifyUser(notifyUser)
                } else {
                    val intent = Intent(this@ActProductDetails, ActWelCome::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            } else {
                val data = recentdataitem
                if (Utils.isLogin(this@ActProductDetails)) {
                    val addtocart = HashMap<String, String>()
                    addtocart["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        )
                            .toString()
                    addtocart["product_id"] = product_id.toString()
                    addtocart["variant_id"] = variant_id.toString()
                    addtocart["qty"] = "1"
                    addtocart["theme_id"] = getString(R.string.theme_id)
                    addtocartApi(
                        addtocart,
                        product_id.toInt(),
                        variant_id.toInt(), 0
                    )
                } else {
                    guestUserAddToCart(data, product_id.toInt(), variant_id, false)
                }
            }
        }
    }

    private fun notifyUser(notifyUser: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .notifyUser(notifyUser)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //Utils.successAlert(this@ActProductDetails,addtocart?.point.toString())
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))

                            this@ActProductDetails.finish()
                            this@ActProductDetails.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }

    }

    private fun dlgAlreadyCart(message: String, product_id: String, i: Int, variant_id: String) {
        val builder = AlertDialog.Builder(this@ActProductDetails)
        builder.setTitle(R.string.app_name)
        val dlgMessage = if (Utils.isLogin(this@ActProductDetails)) {
            message
        } else {
            resources.getString(R.string.product_already_again)
        }
        builder.setMessage(dlgMessage)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActProductDetails)) {
                cartqty["product_id"] = product_id
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActProductDetails,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = variant_id
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = resources.getString(R.string.theme_id)
                cartQtyApi(cartqty)
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActProductDetails,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i].qty ?: 0
                cartList[i].qty = count.plus(1)
                manageOfflineData(cartList)

            }
            dialogInterface.dismiss()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()

        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
    ) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActProductDetails,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                cartQtyResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))

                            this@ActProductDetails.finish()
                            this@ActProductDetails.finishAffinity()

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActProductDetails, "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActProductDetails,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActProductDetails,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActProductDetails)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            startActivity(Intent(this@ActProductDetails, ActShoppingCart::class.java))
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun guestUserAddToCart(
        data: RecentProductDataItem,
        id: Int?,
        variantId: String?,
        fromRelatedItem: Boolean,
    ) {
        Log.e("Position", id.toString())
        val cartData = cartData(data, fromRelatedItem)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            this@ActProductDetails,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        val cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            dlgConfirm(cartData.name + " " + R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, id.toString(), variantId.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id && cartList[i].variantId == variantId) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(resources.getString(R.string.product_already_again),
                        data.id.toString(),
                        i,
                        data.defaultVariantId.toString())
                }
            }
        } else {
            dlgConfirm(cartData.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun cartData(data: RecentProductDataItem, fromRelatedItem: Boolean): ProductListItem {

        return if (fromRelatedItem) {
            ProductListItem(
                data.coverImagePath.toString(),
                data.defaultVariantId.toString(),
                data.finalPrice.toString(),
                data.discountPrice.toString(),
                data.id.toString(),
                1,
                data.name.toString(),
                "",
                "",
                "",
                data.originalPrice.toString()
            )
        } else {
            ProductListItem(
                productInfo.coverImagePath.toString(),
                variant_id,
                finalPrice,
                disCountPrice,
                productInfo.id.toString(),
                1,
                productInfo.name.toString(),
                "",
                "",
                variantName,
                originalPrice
            )
        }
    }

    private fun isProductAlreadyAdded(
        cartList: ArrayList<ProductListItem>,
        id: String,
        variantId: String?,
    ): Boolean {
        return cartList.filter { it.productId == id.toString() && it.variantId == variantId }.size == 1
    }


    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?,
    ) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActProductDetails,
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            if (response.body.isOutOFStock == 1) {
                                Utils.errorAlert(this@ActProductDetails,
                                    response.body.data?.message.toString())
                            } else {
                                dlgAlreadyCart(response.body.data?.message.toString(),
                                    id.toString(),
                                    i!!,
                                    defaultVariantId.toString())
                            }

                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))

                            this@ActProductDetails.finish()
                            this@ActProductDetails.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    /* private fun callVariantApi(variant_sku: String) {
         Utils.showLoadingProgress(this@ActProductDetails)
         val productDetailRequest = HashMap<String, String>()
         productDetailRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
         productDetailRequest["variant_sku"] = variant_sku
         lifecycleScope.launch {
             when (val response =
                 ApiClient.getClient(this@ActProductDetails).variantStock(productDetailRequest)) {
                 is NetworkResponse.Success -> {
                     Utils.dismissLoadingProgress()
                     if (response.body.status == 1) {
                         val data = response.body.data
                         _binding.tvProductPrice.text = Utils.getPrice(data?.finalPrice.toString())
                         _binding.tvCurrency.text = SharePreference.getStringPref(this@ActProductDetails,SharePreference.currency_name).toString()
                         _binding.tvDiscountPrice.text =
                             Utils.getPrice(data?.originalPrice.toString())
                         variant_id = data?.id.toString()
                         disCountPrice = data?.discountPrice.toString()
                         originalPrice = data?.originalPrice.toString()
                         finalPrice = data?.finalPrice.toString()
                         variantName = data?.variant.toString()
                         productStock = data?.stock.toString()

                         if (data?.stock == 0) {*//*
                            _binding.btnAddtoCard.isEnabled = false
                            _binding.btnAddtoCard.isClickable = false*//*
                            *//*Utils.errorAlert(
                                this@ActProductDetails,
                                resources.getString(R.string.out_of_stock)
                            )*//*
                            _binding.btnAddtoCard.text = getString(R.string.notify_me)
                            _binding.tvOutStock.show()

                        } else {
                            *//*_binding.btnAddtoCard.isEnabled = true
                            _binding.btnAddtoCard.isClickable = true*//*
                            _binding.tvOutStock.hide()
                            _binding.btnAddtoCard.text = getString(R.string.add_to_cart)

                        }
                    }

                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }

        }

    }*/

    private fun callProductDetailApi() {
        Utils.showLoadingProgress(this@ActProductDetails)
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActProductDetails, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetails)
                        .productDetailGuest(productDetailRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetails).productDetail(productDetailRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        response.body.data?.let { setupDetailData(it) }
                        productInfo = response.body.data?.productInfo!!
                        product_id = response.body.data.productInfo.id.toString()
                        variant_id = response.body.data.productInfo.defaultVariantId.toString()
                        variantName = response.body.data.productInfo.defaultVariantName.toString()
                        disCountPrice = response.body.data?.productInfo?.discountPrice.toString()
                        originalPrice = response.body.data?.productInfo?.originalPrice.toString()
                        finalPrice = response.body.data?.productInfo?.finalPrice.toString()
                        _binding.tvReturn.text =
                            response.body.data?.productInfo?.retuen_text.toString()
                        if (response.body.data?.productInfo?.is_review == 0) {
                            if (Utils.isLogin(this@ActProductDetails)) {
                                _binding.btnFeedback.show()
                            } else {
                                _binding.btnFeedback.hide()
                            }
                        } else {
                            _binding.btnFeedback.hide()
                        }
                        productStock = response.body.data.productInfo.productStock.toString()
                        if (response.body.data.productInfo.productStock == 0) {

                            _binding.btnAddtoCard.text = getString(R.string.notify_me)
                            _binding.tvOutStock.show()
                        } else {
                            _binding.tvOutStock.hide()
                            _binding.btnAddtoCard.text = getString(R.string.add_to_cart)
                        }
                    }


                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun adapterSetup(imageList: ArrayList<ProductImageItem>) {
        adapter = ViewPagerAdapter(imageList, this@ActProductDetails)
        _binding.viewPager.adapter = adapter
        _binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                setCurrentIndicator(i)
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
        setupIndicator()
    }

    private fun setupIndicator() {
        _binding.indicatorContainer.removeAllViews()
        val indicators = arrayOfNulls<ImageView>(adapter.count)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.apply {
                this.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_default
                    )
                )
                this.layoutParams = layoutParams
            }
            _binding.indicatorContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val childCount = _binding.indicatorContainer.childCount
        for (i in 0 until childCount) {
            val imageview = _binding.indicatorContainer[i] as ImageView
            if (i == index) {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_selected
                    )
                )
            } else {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_default
                    )
                )

            }
        }
    }

}

