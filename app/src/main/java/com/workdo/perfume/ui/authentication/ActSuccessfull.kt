package com.workdo.perfume.ui.authentication

import android.view.View
import com.workdo.perfume.base.BaseActivity
import com.workdo.perfume.databinding.ActSuccessfullBinding
import com.workdo.perfume.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {

    }
}
