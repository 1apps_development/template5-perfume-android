package com.workdo.perfume.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.perfume.databinding.ActWishListBinding

class ActWishList : AppCompatActivity() {
    private lateinit var binding:ActWishListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActWishListBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}