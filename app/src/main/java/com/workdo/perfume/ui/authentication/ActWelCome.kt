package com.workdo.perfume.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.perfume.base.BaseActivity
import com.workdo.perfume.databinding.ActWelcomeBinding
import com.workdo.perfume.ui.activity.MainActivity

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelcomeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelcomeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }
    }

}