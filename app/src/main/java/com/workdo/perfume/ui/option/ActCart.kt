package com.workdo.perfume.ui.option

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.perfume.R
import com.workdo.perfume.databinding.ActCartBinding
import com.workdo.perfume.ui.activity.ActShoppingCart

class ActCart : AppCompatActivity() {
    private lateinit var binding:ActCartBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActCartBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}