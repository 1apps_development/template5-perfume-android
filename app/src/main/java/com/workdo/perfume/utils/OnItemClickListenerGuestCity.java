package com.workdo.perfume.utils;

import com.workdo.perfume.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
