package com.workdo.perfume.utils;

import com.workdo.perfume.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
