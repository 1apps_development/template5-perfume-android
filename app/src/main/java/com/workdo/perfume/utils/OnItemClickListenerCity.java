package com.workdo.perfume.utils;

import com.workdo.perfume.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
