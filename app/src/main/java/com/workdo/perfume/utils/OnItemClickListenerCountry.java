package com.workdo.perfume.utils;

import com.workdo.perfume.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
