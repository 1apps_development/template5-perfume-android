package com.workdo.perfume.utils;

import com.workdo.perfume.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
