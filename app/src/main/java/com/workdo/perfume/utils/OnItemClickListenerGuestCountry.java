package com.workdo.perfume.utils;

import com.workdo.perfume.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
