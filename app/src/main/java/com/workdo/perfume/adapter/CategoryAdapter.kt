package com.workdo.perfume.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.perfume.R
import com.workdo.perfume.api.ApiClient
import com.workdo.perfume.databinding.CellBestCategoriesBinding
import com.workdo.perfume.databinding.CellProducthomeBinding
import com.workdo.perfume.model.CategoryDataItem
import com.workdo.perfume.model.FeaturedProductsSub
import com.workdo.perfume.ui.activity.ActCategoryProduct
import com.workdo.perfume.utils.Constants
import com.workdo.perfume.utils.ExtensionFunctions.invisible
import com.workdo.perfume.utils.ExtensionFunctions.show
import com.workdo.perfume.utils.SharePreference
import com.workdo.perfume.utils.Utils

class CategoryAdapter (
    private val context: Activity,
    private val providerList: ArrayList<CategoryDataItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.BestSellerViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class BestSellerViewHolder(private val binding: CellBestCategoriesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: CategoryDataItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .into(binding.ivCategory)

            binding.tvCategoriesName.text=data.name.toString()

          /*  binding.tvShowMore.setOnClickListener {
                val intent = Intent(context, ActCategoryProduct::class.java)
                intent.putExtra("maincategory_id",data.maincategoryId.toString())
                startActivity(intent)
            }*/

            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view =
            CellBestCategoriesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return BestSellerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}