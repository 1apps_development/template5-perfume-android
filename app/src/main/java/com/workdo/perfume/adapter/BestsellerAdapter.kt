package com.workdo.perfume.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.perfume.R
import com.workdo.perfume.api.ApiClient
import com.workdo.perfume.databinding.CellProducthomeBinding
import com.workdo.perfume.model.FeaturedProductsSub
import com.workdo.perfume.utils.Constants
import com.workdo.perfume.utils.ExtensionFunctions.hide
import com.workdo.perfume.utils.ExtensionFunctions.invisible
import com.workdo.perfume.utils.ExtensionFunctions.show
import com.workdo.perfume.utils.SharePreference
import com.workdo.perfume.utils.Utils

class BestsellerAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<BestsellerAdapter.BestSellerViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class BestSellerViewHolder(private val binding: CellProducthomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivfeaturedProduct)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
           binding.tvCurrency.text = currency

            //binding.tvTag.text = data.tagApi

            if (data.discountType == "percentage") {
                binding.tvDiscount.text = data.discountPrice.toString().plus("%")
            } else {
                binding.tvDiscount.text = currency.plus(Utils.getPrice(data.discountPrice.toString()))

            }

            if (data.inWhishlist== true) {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishList.show()
            }else
            {
                binding.ivWishList.invisible()

            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view =
            CellProducthomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return BestSellerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}