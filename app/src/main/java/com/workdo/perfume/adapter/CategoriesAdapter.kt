package com.workdo.perfume.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.perfume.R
import com.workdo.perfume.model.CategoryDataItem

class CategoriesAdapter(
    var context: Activity,
    private val featuredList: List<CategoryDataItem>,
    private val onClick: (String, String, String) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.viewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_best_sellers, parent, false)

        return viewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val categoriesModel = featuredList[position]

        if (categoriesModel.isSelect) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_red_corner_75, null)
            holder.textView.setTextColor(getColor(context, R.color.white))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_appcolor_75, null)
            holder.textView.setTextColor(getColor(context, R.color.appcolor))
        }
        holder.textView.text = categoriesModel.name

        holder.itemView.setOnClickListener {
            for (element in featuredList) {
                element.isSelect = false
            }
            categoriesModel.isSelect = true
            notifyDataSetChanged()

            onClick(
                categoriesModel.id.toString(),
                categoriesModel.name.toString(),
                categoriesModel.maincategoryId.toString()
            )
        }
    }

    override fun getItemCount(): Int {
        return featuredList.size
    }

    class viewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.tvMandarin)
        val card: ConstraintLayout = itemView.findViewById(R.id.cl)
    }

    private fun onClick(id: String, name: String, mainId: String) {
        onClick.invoke(id, name, mainId)
    }
}